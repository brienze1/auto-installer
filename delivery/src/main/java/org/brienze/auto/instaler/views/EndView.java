package org.brienze.auto.instaler.views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.brienze.auto.instaler.actions.FrameDragListener;
import org.brienze.auto.instaler.controller.ShutDownController;
import org.brienze.auto.instaler.enums.ImagesEnum;
import org.brienze.auto.instaler.service.RunJarService;
import org.brienze.auto.instaler.service.ShortCutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EndView {

	private static JFrame frame;
	private JLabel lblInstallerBackground;
	private JLabel lblBemVindo;
	private JLabel lblVersion;
	private JPanel panel;
	private JButton btnConcluir;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel panel_3;
	
	@Autowired
	private ShutDownController shutDownController;
	
	@Autowired
	private FrameDragListener frameDragListener;

	@Autowired
	private RunJarService runJarService;
	
	@Autowired
	private ShortCutService shortCutService;
	
	@Value("${app.name}")
	private String appName;
	
	@Value("${app.version}")
	private String appVersion;
	
	private JCheckBox chckbxExecutar;
	private JCheckBox chckbxCriarAtalho;
	private String destino;
	
	public EndView() {
		frame = new JFrame();
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public void show(Point point) {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setTitle(appName +  " Programa de Instalação");
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(ImagesEnum.INSTALLER_ICON.getValue()));
		
		panel = new JPanel();
		panel.setBounds(0, 0, 471, 384);
		frame.setBounds(100, 100, panel.getWidth(), panel.getHeight());
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		panel_1 = new JPanel();
		panel_1.setSize(new Dimension(450, 125));
		panel_1.setBackground(SystemColor.control);
		panel_1.setBounds(0, 283, 471, 64);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		btnConcluir = new JButton("Concluir");
		btnConcluir.setBounds(366, 20, 75, 23);
		panel_1.add(btnConcluir);
		
		lblInstallerBackground = new JLabel();
		lblInstallerBackground.setSize(new Dimension(150, 267));
		lblInstallerBackground.setBounds(0, 0, 150, 282);
		lblInstallerBackground.setIcon(ImagesEnum.COMPUTER_BACKGROUND.getValue(lblInstallerBackground.getWidth(), lblInstallerBackground.getHeight()));
		panel.add(lblInstallerBackground);
		
		lblBemVindo = new JLabel("<html>Finalizando o Assistente de Instalação do " + appName + " " +  appVersion + "</html>");
		lblBemVindo.setFont(new Font("Arial", Font.BOLD, 14));
		lblBemVindo.setToolTipText("");
		lblBemVindo.setVerticalAlignment(SwingConstants.TOP);
		lblBemVindo.setHorizontalAlignment(SwingConstants.LEFT);
		lblBemVindo.setBounds(178, 24, 249, 64);
		panel.add(lblBemVindo);
		
		lblVersion = new JLabel("<html>"
				+ "O Programa de Instalação terminou de instalar "+ appName + " " +  appVersion + " no seu computador. O programa pode ser iniciado clicando nos icones instalados."
				+ "<br/><br/>\r\n"
				+ "Clique em Concluir para sair do Programa de Instalação."
				+ "<br/><br/>\r\n"
				+ "</html>");
		lblVersion.setHorizontalAlignment(SwingConstants.LEFT);
		lblVersion.setVerticalTextPosition(SwingConstants.TOP);
		lblVersion.setVerticalAlignment(SwingConstants.TOP);
		lblVersion.setBounds(178, 85, 259, 89);
		panel.add(lblVersion);
		
		panel_2 = new JPanel();
		panel_2.setSize(new Dimension(304, 267));
		panel_2.setBackground(Color.WHITE);
		panel_2.setBounds(149, 0, 322, 282);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		chckbxCriarAtalho = new JCheckBox("Criar atalho na Area de Trabalho");
		chckbxCriarAtalho.setSelected(true);
		chckbxCriarAtalho.setBackground(Color.WHITE);
		chckbxCriarAtalho.setBounds(26, 190, 250, 23);
		panel_2.add(chckbxCriarAtalho);
		
		chckbxExecutar = new JCheckBox("Executar " + appName);
		chckbxExecutar.setSelected(true);
		chckbxExecutar.setBackground(Color.WHITE);
		chckbxExecutar.setBounds(26, 216, 250, 23);
		panel_2.add(chckbxExecutar);
		
		panel_3 = new JPanel();
		panel_3.setBounds(0, 0, 150, 282);
		panel.add(panel_3);
		
		// Actions
		addActions();

		if (point != null) {
			frame.setLocation(point);
		}
		frame.setVisible(true);
	}

	private void addActions() {
		frameDragListener = new FrameDragListener(frame);
		frame.addMouseListener(frameDragListener);
		frame.addMouseMotionListener(frameDragListener);
		
		//Concluir
		btnConcluir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(chckbxCriarAtalho.isSelected()) {
					shortCutService.create(new File(System.getProperty("user.home") + "/Desktop"), new File(destino + "\\" + appName + ".jar"), ImagesEnum.APP_ICON.getName());
					System.out.println("atalho criado");
				}
				if(chckbxExecutar.isSelected()) {
					runJarService.run(new File(destino + "\\" + appName + ".jar"));
					System.out.println("app executado");
				}
				
				shutDownController.exit();
				frame.dispose();
			}
		});
	}

	public void setInstallationFolder(String destino) {
		this.destino = destino;
	}

}
