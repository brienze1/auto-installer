package org.brienze.auto.instaler.views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.brienze.auto.instaler.actions.FrameDragListener;
import org.brienze.auto.instaler.controller.ShutDownController;
import org.brienze.auto.instaler.enums.ImagesEnum;
import org.brienze.auto.instaler.port.ProcessBarAdapter;
import org.brienze.auto.instaler.service.InstalationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class InstalationView implements ProcessBarAdapter {

	private static JFrame frame;
	private static JProgressBar progressBar;
	private JButton btnAvancar;
	private JPanel panel;
	private JButton btnCancelar;
	private JPanel panel_1;
	private JPanel panel_2;
	private String destino;

	@Autowired
	private ShutDownController shutDownController;

	@Autowired
	private FrameDragListener frameDragListener;

	@Autowired
	private InstalationService instalationService;
	
	@Autowired
	private EndView endView;
	
	@Value("${app.name}")
	private String appName;
	
	public InstalationView() {
		frame = new JFrame();
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public void show(Point point) {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setTitle(appName +  " Programa de Instalação");
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(ImagesEnum.INSTALLER_ICON.getValue()));
		
		panel = new JPanel();
		panel.setBounds(0, 0, 471, 384);
		frame.setBounds(100, 100, panel.getWidth(), panel.getHeight());
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		panel_1 = new JPanel();
		panel_1.setSize(new Dimension(450, 125));
		panel_1.setBackground(SystemColor.control);
		panel_1.setBounds(0, 283, 471, 64);
		panel.add(panel_1);
		panel_1.setLayout(null);

		btnAvancar = new JButton("Avançar");
		btnAvancar.setBounds(281, 20, 75, 23);
		btnAvancar.setVisible(false);
		panel_1.add(btnAvancar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(366, 20, 75, 23);
		panel_1.add(btnCancelar);

		panel_2 = new JPanel();
		panel_2.setSize(new Dimension(304, 267));
		panel_2.setBackground(Color.WHITE);
		panel_2.setBounds(0, 0, 471, 282);
		panel.add(panel_2);
		panel_2.setLayout(null);

		progressBar = new JProgressBar();
		progressBar.setBounds(32, 34, 394, 17);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		panel_2.add(progressBar);

		// Actions
		addActions();

		if (point != null) {
			frame.setLocation(point);
		}
		frame.setVisible(true);
		install();
	}

	private void addActions() {
		frameDragListener = new FrameDragListener(frame);
		frame.addMouseListener(frameDragListener);
		frame.addMouseMotionListener(frameDragListener);

		// Avancar
		btnAvancar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("avancado");
				endView.setInstallationFolder(destino);
				endView.show(frame.getLocation());
				frame.dispose();
			}
		});

		// Cancelar
		btnCancelar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (JOptionPane.showConfirmDialog(null,
						"Você tem certeza que deseja sair? A instalação será cancelada.", "Confirmação",
						JOptionPane.YES_NO_OPTION) == 0) {
					shutDownController.exit();
					frame.dispose();
				}
			}
		});
	}
	
	public void setInstallationFolder(String destino) {
		this.destino = destino;
	}
	
	// function to increase progress
	private void install() {
		new Thread() {
			@Override
			public void run() {
				instalationService.install(destino);
			}
		}.start();
	}

	@Override
	public void fill(Integer value) {
		progressBar.setValue(getProgresBarValue()+value);
		
		if(progressBar.getValue()>=100) {
			btnAvancar.setVisible(true);
			btnCancelar.setVisible(false);
		}
	}
	
	@Override
	public Integer getProgresBarValue() {
		return progressBar.getValue();
	}

}
