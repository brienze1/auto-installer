package org.brienze.auto.instaler.views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.brienze.auto.instaler.actions.FrameDragListener;
import org.brienze.auto.instaler.controller.ShutDownController;
import org.brienze.auto.instaler.enums.ImagesEnum;
import org.brienze.auto.instaler.port.WindowsSuperUserDetectorAdapter;
import org.brienze.auto.instaler.service.RunJarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class HomeView {
	
	private static JFrame frame;
	private JLabel lblInstallerBackground;
	private JLabel lblBemVindo;
	private JLabel lblVersion;
	private JPanel panel;
	private JButton btnCancelar;
	private JButton btnAvancar;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel panel_3;
	
	@Autowired
	private WindowsSuperUserDetectorAdapter windowsSuperUserDetectorAdapter;
	
	@Autowired
	private ShutDownController shutDownController;
	
	@Autowired
	private FrameDragListener frameDragListener;
	
	@Autowired
	private SelectInstalationFolderView selectInstalationFolderView; 
	
	@Autowired
	private RunJarService runJarService;
	
	@Value("${app.name}")
	private String appName;
	
	@Value("${app.version}")
	private String appVersion;
	
	public HomeView() {
		frame = new JFrame();
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public void show(Point point) {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setTitle(appName +  " Programa de Instalação");
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(ImagesEnum.INSTALLER_ICON.getValue()));
		
		panel = new JPanel();
		panel.setBounds(0, 0, 471, 384);
		frame.setBounds(100, 100, panel.getWidth(), panel.getHeight());
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		panel_1 = new JPanel();
		panel_1.setSize(new Dimension(450, 125));
		panel_1.setBackground(SystemColor.control);
		panel_1.setBounds(0, 283, 471, 64);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		btnAvancar = new JButton("Avançar");
		btnAvancar.setBounds(281, 20, 75, 23);
		panel_1.add(btnAvancar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(366, 20, 75, 23);
		panel_1.add(btnCancelar);
		
		lblInstallerBackground = new JLabel();
		lblInstallerBackground.setSize(new Dimension(150, 267));
		lblInstallerBackground.setBounds(0, 0, 150, 282);
		lblInstallerBackground.setIcon(ImagesEnum.COMPUTER_BACKGROUND.getValue(lblInstallerBackground.getWidth(), lblInstallerBackground.getHeight()));
		panel.add(lblInstallerBackground);
		
		lblBemVindo = new JLabel("<html>Bem-vindo ao Assistente de Instalação do " + appName + "</html>");
		lblBemVindo.setFont(new Font("Arial", Font.BOLD, 14));
		lblBemVindo.setToolTipText("");
		lblBemVindo.setVerticalAlignment(SwingConstants.TOP);
		lblBemVindo.setHorizontalAlignment(SwingConstants.LEFT);
		lblBemVindo.setBounds(178, 24, 249, 64);
		panel.add(lblBemVindo);
		
		lblVersion = new JLabel("<html>"
				+ "Este Assistente irá instalar "+ appName + appVersion + " no seu computador."
				+ "<br/><br/>\r\n"
				+ "É recomendado que você feche todos os outros aplicativos antes de continuar."
				+ "<br/><br/>\r\n"
				+ "Clique em Avançar para continuar, ou em Cancelar para sair do Programa de Instalação."
				+ "</html>");
		lblVersion.setHorizontalAlignment(SwingConstants.LEFT);
		lblVersion.setVerticalTextPosition(SwingConstants.TOP);
		lblVersion.setVerticalAlignment(SwingConstants.TOP);
		lblVersion.setBounds(178, 85, 249, 171);
		panel.add(lblVersion);
		
		panel_2 = new JPanel();
		panel_2.setSize(new Dimension(304, 267));
		panel_2.setBackground(Color.WHITE);
		panel_2.setBounds(149, 0, 322, 282);
		panel.add(panel_2);
		
		panel_3 = new JPanel();
		panel_3.setBounds(0, 0, 150, 282);
		panel.add(panel_3);
		
		// Actions
		addActions();

		if(!isSuperUser()) {
			runJarService.run(new File(System.getProperty("user.dir") + "\\instalador.jar"));
			shutDownController.exit();
		} else {
			if (point != null) {
				frame.setLocation(point);
			}
			frame.setVisible(true);
		}
	}

	private boolean isSuperUser() {
		return windowsSuperUserDetectorAdapter.isSuperUser();
	}

	private void addActions() {
		frameDragListener = new FrameDragListener(frame);
		frame.addMouseListener(frameDragListener);
		frame.addMouseMotionListener(frameDragListener);
		
		//Avancar
		btnAvancar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("avancado");
				selectInstalationFolderView.show(frame.getLocation());
				frame.dispose();
			}
		});
		
		//Cancelar
		btnCancelar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(JOptionPane.showConfirmDialog(null, "Você tem certeza que deseja sair? A instalação será cancelada.", "Confirmação", JOptionPane.YES_NO_OPTION) == 0) {
					shutDownController.exit();
					frame.dispose();
				}
			}
		});
	}
}
