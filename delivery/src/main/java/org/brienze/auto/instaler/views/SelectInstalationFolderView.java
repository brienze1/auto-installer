package org.brienze.auto.instaler.views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.brienze.auto.instaler.actions.FrameDragListener;
import org.brienze.auto.instaler.controller.ShutDownController;
import org.brienze.auto.instaler.enums.ImagesEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SelectInstalationFolderView {

	private static JFrame frame;
	private JLabel lblInstallerBackground;
	private JLabel lblBemVindo;
	private JLabel lblVersion;
	private JPanel panel;
	private JButton btnCancelar;
	private JButton btnInstalar;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel panel_3;
	private JTextField textDestino;
	private JLabel lblDiretorioDeDestino;
	private JButton btnBuscar;

	@Autowired
	private ShutDownController shutDownController;

	@Autowired
	private FrameDragListener frameDragListener;
	
	@Autowired
	private InstalationView instalationView;
	
	@Value("${app.name}")
	private String appName;
	
	public SelectInstalationFolderView() {
		frame = new JFrame();
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public void show(Point point) {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setTitle(appName +  " Programa de Instalação");
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(ImagesEnum.INSTALLER_ICON.getValue()));
		
		panel = new JPanel();
		panel.setBounds(0, 0, 471, 384);
		frame.setBounds(100, 100, panel.getWidth(), panel.getHeight());
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		panel_1 = new JPanel();
		panel_1.setSize(new Dimension(450, 125));
		panel_1.setBackground(SystemColor.control);
		panel_1.setBounds(0, 283, 471, 64);
		panel.add(panel_1);
		panel_1.setLayout(null);

		btnInstalar = new JButton("Instalar");
		btnInstalar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnInstalar.setBounds(281, 20, 75, 23);
		panel_1.add(btnInstalar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(366, 20, 75, 23);
		panel_1.add(btnCancelar);

		lblInstallerBackground = new JLabel();
		lblInstallerBackground.setSize(new Dimension(150, 267));
		lblInstallerBackground.setBounds(0, 0, 150, 282);
		lblInstallerBackground.setIcon(ImagesEnum.COMPUTER_BACKGROUND.getValue(lblInstallerBackground.getWidth(), lblInstallerBackground.getHeight()));
		panel.add(lblInstallerBackground);

		lblBemVindo = new JLabel("<html>Selecione a Pasta de Destino</html>");
		lblBemVindo.setFont(new Font("Arial", Font.BOLD, 14));
		lblBemVindo.setToolTipText("");
		lblBemVindo.setVerticalAlignment(SwingConstants.TOP);
		lblBemVindo.setHorizontalAlignment(SwingConstants.LEFT);
		lblBemVindo.setBounds(178, 24, 249, 64);
		panel.add(lblBemVindo);

		lblVersion = new JLabel("<html>"
				+ "Aperte o botão <strong>Instalar</strong> para iniciar a instalação."
				+ "<br/><br/>\r\n"
				+ "Use o botão <strong>Buscar</strong> para selecionar a pasta de destino. Também pode especificar o caminho manualmente."
				+ "<br/><br/>\r\n"
				+ "Caso a pasta de destino não exista, será criada automaticamente antes da instalação."
				+ "</html>");
		lblVersion.setHorizontalAlignment(SwingConstants.LEFT);
		lblVersion.setVerticalTextPosition(SwingConstants.TOP);
		lblVersion.setVerticalAlignment(SwingConstants.TOP);
		lblVersion.setBounds(178, 85, 263, 112);
		panel.add(lblVersion);

		panel_2 = new JPanel();
		panel_2.setSize(new Dimension(304, 267));
		panel_2.setBackground(Color.WHITE);
		panel_2.setBounds(149, 0, 322, 282);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		textDestino = new JTextField();
		textDestino.setText(new File(System.getenv("ProgramFiles")).getAbsolutePath() + "\\" + appName);
		textDestino.setBounds(25, 251, 179, 20);
		panel_2.add(textDestino);
		textDestino.setColumns(10);
		
		btnBuscar = new JButton("Buscar ...");
		btnBuscar.setBounds(214, 250, 77, 23);
		panel_2.add(btnBuscar);
		
		lblDiretorioDeDestino = new JLabel("Diretorio de destino");
		lblDiretorioDeDestino.setBounds(25, 233, 97, 14);
		panel_2.add(lblDiretorioDeDestino);

		panel_3 = new JPanel();
		panel_3.setBounds(0, 0, 150, 282);
		panel.add(panel_3);

		// Actions
		addActions();

		if (point != null) {
			frame.setLocation(point);
		}
		frame.setVisible(true);
	}

	private void addActions() {
		frameDragListener = new FrameDragListener(frame);
		frame.addMouseListener(frameDragListener);
		frame.addMouseMotionListener(frameDragListener);
		
		//Buscar
		btnBuscar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFileChooser jfc = new JFileChooser(System.getenv("ProgramFiles"));
				jfc.setDialogTitle("Choose a directory to save your file: ");
				jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

				int returnValue = jfc.showSaveDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					if (jfc.getSelectedFile().isDirectory()) {
						System.out.println("You selected the directory: " + jfc.getSelectedFile());
						textDestino.setText(jfc.getSelectedFile().getAbsolutePath());
					}
				}
			}
		});

		// Instalar
		btnInstalar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(!textDestino.getText().isBlank() && textDestino.getText().startsWith(new File(System.getProperty("user.home")).toPath().getRoot().toString())) {
					instalationView.setInstallationFolder(textDestino.getText());
					instalationView.show(frame.getLocation());
					frame.dispose();
				} else {
					JOptionPane.showMessageDialog(null, "Este não é um destino valido, favor inserir outro e tentar novamente.");
				}  
			}
		});

		// Cancelar
		btnCancelar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (JOptionPane.showConfirmDialog(null,	"Você tem certeza que deseja sair? A instalação será cancelada.", "Confirmação", JOptionPane.YES_NO_OPTION) == 0) {
					shutDownController.exit();  
					frame.dispose();
				}
			}
		});
	}
}
