package org.brienze.auto.instaler.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;

import org.brienze.auto.instaler.exception.UserAlreadyExistsExceptionExample;

@ControllerAdvice
public class ErrorHandler {

	@ExceptionHandler({UserAlreadyExistsExceptionExample.class})
	public ResponseEntity<Object> errorHandler(Exception ex) {
		throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());
	}
	
}
