Aplicativo desktop para instalacao de jars.

Separado em 4 camadas:
   *  Application -> Configuracao dos beans do projeto e properties.
   *  Delivery -> Camada exposta das views.
   *  Domain -> Camada de servico.
   *  Integration -> Camada de integracao da API com arquivos.
  
Proximos passos:

*  Adicionar testes unitarios para as camadas
*  Adicionar teste integrado com cucumber

Feito por Luis Brienze. 
Linkedin: https://www.linkedin.com/in/luis.brienze/
GitLab: https://gitlab.com/brienze1