package org.brienze.auto.instaler.webservice;

import java.io.File;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.stereotype.Component;

@Component
public class GitWebService implements org.brienze.auto.instaler.port.GitWebServiceAdapter {
	
//	@Autowired
//	private FileUtils fileUtils;

	public File downloadProject(String gitUrl, File repository) {
		try {
			Git git = Git.cloneRepository()
					.setURI(gitUrl)
					.setDirectory(repository)
					.call();
			git.close();
		} catch (IllegalStateException | GitAPIException e) {
			// TODO: criar exception
			e.printStackTrace();
			throw new RuntimeException();
		}
		//necessario a pasta .git para updates futuros
//		fileUtils.delete(new File(repository + "/.git"));
		
		return repository;
	}

}
