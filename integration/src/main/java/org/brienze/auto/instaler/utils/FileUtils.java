package org.brienze.auto.instaler.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

import org.brienze.auto.instaler.port.FileUtilsAdapter;
import org.springframework.stereotype.Component;

import mslinks.ShellLink;

@Component
public class FileUtils implements FileUtilsAdapter {

	@Override
	public File createRepository(String destination) {
		File file = new File(destination);
		if (file.exists()) {
			return file;
		} else if (file.mkdirs()) {
			return file;
		} else {
			try {
				Files.createDirectory(file.toPath());
				return file;
			} catch (IOException e) {
				// TODO: criar exception
				e.printStackTrace();
				throw new RuntimeException("Erro ao tentar criar repositorio");
			}
		}
	}

	@Override
	public File renameFile(File source, String newName) {
		Path file = Paths.get(source.getAbsolutePath());

		try {
			return new File(Files.move(file, file.resolveSibling(newName)).getParent() + File.separator + newName);
		} catch (IOException e) {
			// TODO criar exception
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	@Override
	public void delete(File fileToDelete) {
		if (fileToDelete.isFile()) {
			if (!fileToDelete.delete()) {
				// TODO: criar exception
				throw new RuntimeException();
			}
			return;
		}

		String[] files = listDirFiles(fileToDelete);

		if (files != null) {
			for (String stringFile : files) {
				File file = new File(stringFile);
				if (file.isDirectory()) {
					delete(file);
				} else {
					if (!file.delete()) {
						// TODO: criar exception
						throw new RuntimeException();
					}
				}
			}
		}

		if (!fileToDelete.delete()) {
			// TODO: criar exception
			throw new RuntimeException();
		}
	}

	@Override
	public String[] listAllRepositories(File fileRepository) {
		File[] files = fileRepository.listFiles();

		List<String> fileNames = new ArrayList<>();
		if (files != null) {
			for (File file : files) {
				if (file.isDirectory()) {
					fileNames.add(file.getName());
					fileNames.addAll(Arrays.asList(listAllRepositories(file)));
				}
			}
		}
		return fileNames.toArray(new String[0]);
	}

	@Override
	public String[] listAllFiles(File fileDir) {
		File[] files = fileDir.listFiles();

		List<String> fileNames = new ArrayList<>();
		if (files != null) {
			for (File file : files) {
				if (file.isDirectory()) {
					fileNames.addAll(Arrays.asList(listAllFiles(file)));
				} else {
					fileNames.add(file.getAbsolutePath());
				}
			}
		}

		return fileNames.toArray(new String[0]);
	}

	@Override
	public String[] listDirFiles(File fileDir) {
		File[] files = fileDir.listFiles();

		List<String> fileNames = new ArrayList<>();
		if (files != null) {
			for (File file : files) {
				fileNames.add(file.getAbsolutePath());
			}
		}

		return fileNames.toArray(new String[0]);
	}

	@Override
	public File copyFile(File file, File destination) {
		try {
			org.apache.commons.io.FileUtils.copyFileToDirectory(file, destination);
		} catch (IOException e) {
			// TODO Gerar erro
			e.printStackTrace();
			throw new RuntimeException();
		}
		return new File(destination.getAbsolutePath() + "\\" + file.getName());
	}

	@Override
	public JarOutputStream makeRunnableJar(File instalationFolder, File appFolder, String appName, String mainClass) {
		Manifest manifest = new Manifest();
		manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
		manifest.getMainAttributes().put(Attributes.Name.MAIN_CLASS, mainClass);
		manifest.getMainAttributes().put(Attributes.Name.CLASS_PATH,
				"file:///" + instalationFolder + "/" + appName + "/application/src/main/java/");
		JarOutputStream target = null;
		try {
			target = new JarOutputStream(new FileOutputStream(instalationFolder + "/" + appName + ".jar"), manifest);
			add(appFolder, target);
		} catch (IOException e) {
			// TODO gerar excecao correta
			e.printStackTrace();
			throw new RuntimeException();
		} finally {
			if (target != null) {
				try {
					target.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException();
				}
			}
		}
		return target;
	}

	private void add(File source, JarOutputStream target) throws IOException {
		BufferedInputStream in = null;
		try {
			if (source.isDirectory()) {
				String name = source.getPath().replace("\\", "/");
				if (!name.isEmpty()) {
					if (!name.endsWith("/")) {
						name += "/";
					}
					JarEntry entry = new JarEntry(name);
					entry.setTime(source.lastModified());
					target.putNextEntry(entry);
					target.closeEntry();
				}
				for (File nestedFile : source.listFiles()) {
					add(nestedFile, target);
				}
				return;
			}

			JarEntry entry = new JarEntry(source.getPath().replace("\\", "/"));
			entry.setTime(source.lastModified());
			target.putNextEntry(entry);
			in = new BufferedInputStream(new FileInputStream(source));

			byte[] buffer = new byte[1024];
			while (true) {
				int count = in.read(buffer);
				if (count == -1)
					break;
				target.write(buffer, 0, count);
			}
			target.closeEntry();
		} finally {
			if (in != null)
				in.close();
		}
	}

	@Override
	public void createShortCut(File destination, File source, File icon) {
		try {
			ShellLink shellLink = ShellLink.createLink(source.getAbsolutePath(), destination.getAbsolutePath() + "\\" + source.getName().replaceAll(".jar", "") + ".lnk");
			shellLink.setIconLocation(icon.getPath());
			shellLink.saveTo(destination.getAbsolutePath() + "\\" + source.getName().replaceAll(".jar", "") + ".lnk");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("Erro na criacao do atalho");
		}
	}

	@Override
	public File extractFile(File zipFile, String fileToBeExtracted, File toDirectory) {
		File responseFile = null;
		try (JarFile jar = new JarFile(zipFile)) {
			Enumeration<JarEntry> enumEntries = jar.entries();
			while (enumEntries.hasMoreElements()) {
				JarEntry file = (JarEntry) enumEntries.nextElement();
				if (file.getName().contains(fileToBeExtracted)) { //se o arquivo tiver o mesmo nome que o arquivo pedido
					File f = new File(toDirectory + File.separator + fileToBeExtracted);
					responseFile = f;
					if (file.isDirectory()) { // if its a directory, create it
						f.mkdir();
						continue;
					}
					try (InputStream is = jar.getInputStream(file)) {
						// get the input stream
						try (FileOutputStream fos = new FileOutputStream(f)) {

							while (is.available() > 0) { // write contents of 'is' to 'fos'
								fos.write(is.read());
							}
						}catch (IOException e) {
							// TODO: handle exception
							e.printStackTrace();
							throw new RuntimeException("Erro ao tentar criar FileOutputStream, erro: " + e.getMessage());
						}
					} catch (IOException e) {
						// TODO: handle exception
						e.printStackTrace();
						throw new RuntimeException("Erro ao tentar criar InputStream, erro: " + e.getMessage());
					}
				}
			}
			return responseFile;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("Erro ao tentar criar JarFile, erro: " + e.getMessage());
		}
	}

}
