package org.brienze.auto.instaler.utils;

import org.brienze.auto.instaler.port.ProcessBarAdapter;
import org.brienze.auto.instaler.port.ProcessBarUpdaterAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProcessBarUpdater implements ProcessBarUpdaterAdapter {

	@Autowired
	private ProcessBarAdapter processBar;
	
	@Override
	public void fill(Integer maxAmount, Integer segundos) {
		if(maxAmount < 0 || maxAmount > 100 || segundos <= 0) {
			//TODO fazer exception
			throw new RuntimeException();
		}
		new Thread() {
			@Override
			public void run() {
				int quantidade = 1;
				int quantidadeAntiga = 1;
				int i = 1;
				while(quantidadeAntiga != 0) {
					quantidade = quantidadeAntiga;
					quantidadeAntiga = (maxAmount - processBar.getProgresBarValue())/(segundos*i);
					i++;
				}
				while(processBar.getProgresBarValue() < maxAmount) {
					try {
						Thread.sleep(1000/(i-1));
						processBar.fill(quantidade);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}
	
	@Override
	public void fill() {
		processBar.fill(100 - processBar.getProgresBarValue());
	}

	@Override
	public void fill(Integer value) {
		processBar.fill(value - processBar.getProgresBarValue());
	}
	
}
