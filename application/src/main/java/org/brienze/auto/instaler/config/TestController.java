package org.brienze.auto.instaler.config;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@GetMapping("/testeconexao")
	public String test() {
		return "teste OK";
	}
	
}
