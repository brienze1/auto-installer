package org.brienze.auto.instaler;

import org.brienze.auto.instaler.views.HomeView;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application implements CommandLineRunner {
	
	public static void main(String[] args) {
		new SpringApplicationBuilder(Application.class).headless(false).run(args);
	}

	@Bean
	public HomeView showInitialScreen() {
		return new HomeView();
	}
	
	@Override
	public void run(String... args) throws Exception {
		showInitialScreen().show(null);
	}

}
