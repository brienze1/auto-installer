package org.brienze.auto.instaler.service;

import java.io.File;
import java.io.IOException;

import org.brienze.auto.instaler.port.FileUtilsAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RunJarService {

	@Autowired
	private FileUtilsAdapter fileUtils;
	
	public void run(File jar) {
		 try {
			String command = "javaw -jar -Xms1024m -Xmx1024m " + jar.getAbsolutePath();
//			String command = "java -jar " + jar.getAbsolutePath();
			System.out.println(command);
			
			String[] listaArquivos =  fileUtils.listDirFiles(jar.getParentFile());
			for (String arquivo : listaArquivos) {
				if(arquivo.contains("elevate")) {
					Runtime.getRuntime().exec(jar.getParentFile() + File.separator + "elevate.exe " + command);
					return;
				}
			}

			
			fileUtils.extractFile(jar, "elevate.exe", jar.getParentFile());
			Runtime.getRuntime().exec(jar.getParentFile() + File.separator + "elevate.exe " + command);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("Erro ao tentar executar o jar");
		}
	}
	
}
