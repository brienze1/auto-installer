package org.brienze.auto.instaler.enums;

import java.awt.Image;
import java.io.File;
import java.net.URL;

import javax.swing.ImageIcon;

public enum ImagesEnum {
	
	INSTALLER_ICON("installer.png"),
	COMPUTER_BACKGROUND("computer_background.png"), 
	APP_ICON("wesave_icon.ico");

	private String name;
	private String PATH = "/org/brienze/auto/instaler/config/imgs/";
	
	ImagesEnum(String name) {
		this.name = name;
	}
	
	public URL getValue() {
		return ImagesEnum.class.getResource(PATH + name);
	}
	
	public File getFile() {
		return new File(ImagesEnum.class.getResource(PATH + name).getFile());
	}
	
	public ImageIcon getValue(Integer width, Integer height) {
		return new ImageIcon(new ImageIcon(ImagesEnum.class.getResource(PATH + name)).getImage().getScaledInstance(width, height, Image.SCALE_DEFAULT));
	}
	
	public String getName() {
		return this.name;
	}

}
