package org.brienze.auto.instaler.port;

import java.io.File;
import java.util.jar.JarOutputStream;

public interface FileUtilsAdapter {

	File createRepository(String destination);

	File renameFile(File source, String newName);

	void delete(File fileToDelete);

	String[] listAllRepositories(File fileRepository);

	String[] listAllFiles(File fileDir);
	
	String[] listDirFiles(File fileDir);

	File copyFile(File file, File destination);
	
	JarOutputStream makeRunnableJar(File instalationFolder, File appFolder, String appName, String mainClass);

	void createShortCut(File destination, File source, File icon);

	File extractFile(File zipFile, String fileToBeExtracted, File toDirectory);

}
