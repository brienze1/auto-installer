package org.brienze.auto.instaler.service;

import java.io.File;

import javax.swing.JOptionPane;

import org.brienze.auto.instaler.enums.ImagesEnum;
import org.brienze.auto.instaler.port.FileUtilsAdapter;
import org.brienze.auto.instaler.port.GitWebServiceAdapter;
import org.brienze.auto.instaler.port.ProcessBarUpdaterAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class InstalationService {
	
	@Autowired
	private GitWebServiceAdapter gitWebService;
	
	@Autowired
	private FileUtilsAdapter fileUtils;
	
	@Autowired
	private ProcessBarUpdaterAdapter processBarUpdater; 
	
	@Value("${app.name}")
	private String appName;
	
	@Value("${app.url.download}")
	private String appDownloadUrl;

	@Value("${app.name.main.class}")
	private String appMainClass;

	@Value("${updater.name}")
	private String updaterName;
	
	@Value("${updater.url.download}")
	private String updaterDownloadUrl;
	
	@Value("${updater.name.main.class}")
	private String updaterMainClass;
	
	private File updateJar;
	private File updateFolder;
	private File installFolder;
	private File installJar;
	private File installIcon;

	public void install(String folder) {
		processBarUpdater.fill(10, 1);
		//verifica se o repositorio de instalacao ja existe
		updateFolder = new File(folder + "/" + updaterName);
		updateJar = new File(folder + "/" + updaterName + ".jar");
		installFolder = new File(folder + "/" + appName);
		installJar = new File(folder + "/" + appName + ".jar");
		installIcon = new File(folder + "/" + ImagesEnum.APP_ICON.getName());
		
		//verifica se o repositorio de instalacao ja existe
		if(installFolder.exists() || updateFolder.exists()) {
			if (JOptionPane.showConfirmDialog(null,	"Parece que o aplicativo ja está instalado, deseja reinstalar?", "Confirmação", JOptionPane.YES_NO_OPTION) == 0) {
				//deleta todos os arquivos da instalacao da pasta para realizar uma instalacao limpa
				deleteAllArchives();
				
				//Cria repo para download do aplicativo e do updater
				processBarUpdater.fill(30, 1);
				fileUtils.createRepository(installFolder.getAbsolutePath());
				fileUtils.createRepository(updateFolder.getAbsolutePath());
				
				//Faz o download do aplicativo e do updater
				processBarUpdater.fill(60, 10);
				gitWebService.downloadProject(appDownloadUrl, installFolder);
				processBarUpdater.fill(90, 10);
				gitWebService.downloadProject(updaterDownloadUrl, updateFolder);
			} else if(installFolder.exists()) {
				//Cria repo para download do updater
				processBarUpdater.fill(30, 1);
				fileUtils.createRepository(updateFolder.getAbsolutePath());
				
				//Faz o download do updater
				processBarUpdater.fill(90, 10);
				gitWebService.downloadProject(updaterDownloadUrl, updateFolder);
			} else if(updateFolder.exists()) {
				//Cria repo para download do aplicativo
				processBarUpdater.fill(30, 1);
				fileUtils.createRepository(installFolder.getAbsolutePath());
				
				//Faz o download do aplicativo
				processBarUpdater.fill(90, 10);
				gitWebService.downloadProject(appDownloadUrl, installFolder);
			}
		} else {
			//Cria repo para download do aplicativo e do updater
			processBarUpdater.fill(30, 1);
			fileUtils.createRepository(installFolder.getAbsolutePath());
			fileUtils.createRepository(updateFolder.getAbsolutePath());
			
			//Faz o download do aplicativo e do updater
			processBarUpdater.fill(60, 10);
			gitWebService.downloadProject(appDownloadUrl, installFolder);
			processBarUpdater.fill(90, 10);
			gitWebService.downloadProject(updaterDownloadUrl, updateFolder);
		}
		processBarUpdater.fill(90);
		
		//deleta somente os outros arquivos, deixa a pasta de instalacao intacta
		deleteArchives();
	
		//transforma a pasta do codigo fonte do app e do updater em um runnable jar
		processBarUpdater.fill(95, 1);
		makeRunnableJar(new File(folder), installFolder, appName);
		File jarUpdater = makeRunnableJar(new File(folder), updateFolder, updaterName);
		
		//Copia elevate.exe para pasta de instalacao
		processBarUpdater.fill(99, 1);
		fileUtils.extractFile(jarUpdater, "elevate.exe", new File(folder));
		
		processBarUpdater.fill();
	}

	private File makeRunnableJar(File parentFolder, File mainFolder, String name) {
		//TODO evoluir para gerador de jar
		String[] files = fileUtils.listAllFiles(new File(mainFolder.getAbsolutePath()+"\\application\\target"));
		
		File jar = null;
		
		for (String file : files) {
			if(file.endsWith(".jar")) {
				jar = new File(mainFolder.getAbsolutePath()+"\\application\\target\\" + new File(file).getName());
			}
		}
		
		File newFile = null;
		if(jar != null) {
			newFile = fileUtils.copyFile(jar, parentFolder);
		} else {
			//TODO erro jar nao encontrado
			throw new RuntimeException();
		}
		
		return fileUtils.renameFile(newFile, name + ".jar");
	}
	
	private void deleteArchives() {
		String[] archives = fileUtils.listDirFiles(installFolder.getParentFile());
		for (String archive : archives) {
			if(!archive.equals(installFolder.getAbsolutePath()) 
					&& !archive.equals(updateFolder.getAbsolutePath()) 
					&& (archive.contains(installJar.getName()) 
							|| archive.contains(installIcon.getName())
							|| archive.contains(updateJar.getName()))) {
				fileUtils.delete(new File(archive));
			}
		}
	}

	private void deleteAllArchives() {
		String[] archives = fileUtils.listDirFiles(installFolder.getParentFile());
		for (String archive : archives) {
			if((archive.equals(installFolder.getAbsolutePath()) 
					|| archive.equals(updateFolder.getAbsolutePath()) 
					|| archive.contains(installJar.getName()) 
					|| archive.contains(installIcon.getName())
					|| archive.contains(updateJar.getName()))) {
				fileUtils.delete(new File(archive));
			}
		}
	}
	
}
