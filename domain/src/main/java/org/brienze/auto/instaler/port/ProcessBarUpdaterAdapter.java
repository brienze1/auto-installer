package org.brienze.auto.instaler.port;

public interface ProcessBarUpdaterAdapter {

	void fill(Integer maxAmount, Integer segundos);

	void fill();

	void fill(Integer value);

}
