package org.brienze.auto.instaler.port;

public interface WindowsSuperUserDetectorAdapter {

	boolean isSuperUser();

}
