package org.brienze.auto.instaler.service;

import java.io.File;

import org.brienze.auto.instaler.enums.ImagesEnum;
import org.brienze.auto.instaler.port.FileUtilsAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ShortCutService {

	@Autowired
	private FileUtilsAdapter fileUtils;
	
	public void create(File destination, File source, String icon) {
		File newIcon;
		try {
			newIcon = fileUtils.extractFile(new File(System.getProperty("user.dir") + "\\instalador.jar"), icon, source.getParentFile());
		} catch (RuntimeException e) {
			newIcon = fileUtils.copyFile(ImagesEnum.APP_ICON.getFile(), source.getParentFile());
		}
		
		fileUtils.createShortCut(destination, source, newIcon);
	}
	
}
