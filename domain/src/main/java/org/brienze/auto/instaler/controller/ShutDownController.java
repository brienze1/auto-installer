package org.brienze.auto.instaler.controller;

import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ShutDownController implements ApplicationContextAware {

	private ApplicationContext context;
	
	public void exit() {
		SpringApplication.exit(context, () -> 0);
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.context = applicationContext;
	}

	
	
}
