package org.brienze.auto.instaler.port;

import java.io.File;

public interface GitWebServiceAdapter {

	public File downloadProject(String gitUrl, File repository);
		
}
