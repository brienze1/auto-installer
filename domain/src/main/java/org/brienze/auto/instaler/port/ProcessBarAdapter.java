package org.brienze.auto.instaler.port;

public interface ProcessBarAdapter {

	void fill(Integer quantidade);

	Integer getProgresBarValue();

}
